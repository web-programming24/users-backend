import { IsNotEmpty, MinLength, IsInt, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @Min(1)
  price: number;
}
