import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'กะเพราหมูกรอบ', price: 50 },
  { id: 2, name: 'ผัดมาม่า', price: 50 },
  { id: 3, name: 'หมาล่า', price: 50 },
];
let lastProductId = 4;

@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct = new Product();
    newProduct.id = lastProductId++;
    newProduct.name = createProductDto.name;
    newProduct.price = createProductDto.price;
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('update' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'กะเพราหมูกรอบ', price: 50 },
      { id: 2, name: 'ผัดมาม่า', price: 50 },
      { id: 3, name: 'หมาล่า', price: 50 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
